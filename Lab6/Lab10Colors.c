//Name of Motors used throughout the Program.
int leftMotor = 1;
int rightMotor = 2;


//This function is responsible for making the motor go forward and backwards.
void drive(long nMotorRatio, float dist, long power)
{
	int encoderRatio = (360/2*3.14* 2.75) * dist ; // EncoderRatio = 360/Circumference * distance
	setMotorSyncEncoder(leftMotor, rightMotor,nMotorRatio, encoderRatio, power);
	sleep((500*dist/25) * (100/power)); //An issue with sleep meant that when changing power the robot would not go the full distance specified, this was fixed by adding distance and power into the function.
	//500*60/25 * 100 will go foward exactly 60cm and if you change power to half speed itll double the time delay to account for the decreased speed.
	
	//Resets Motor Speed as a precaution.
	setMotorSpeed(leftMotor, 0);
	setMotorSpeed(rightMotor, 0);
	
}



//This function extends drive and is used to turn left at a given power.
void turn90right(int power)
{	
	int currenthead;
	resetGyro(S1);
	currenthead = getGyroDegrees(S1);
	
	while (-getGyroDegrees(S1) <= currenthead+87)
	{
		sleep(100);
		drive(-100,0.25,100);
		
	}
	
}

//Function Responsible for displaying colour on the controller screen.
void displayColor(string color){
	//Clears whatever is currently on the display.
	eraseDisplay();
	//Displays the new colour
	displayTextLine(1 ,"Color: %s",color);
	wait1Msec(5000);
}
task main(){
	//String variable which contains the current colour.
	string color ="";
	//Declaring Sensors
	SensorType[S3] = sensorEV3_Color;
	SensorMode[S3] = modeEV3Color_Color;
	SensorType[S1] = sensorEV3_Gyro;
	
	//While true this loop is continiously run.
	while (1){
		//If the colour is blue it displays blue and turns right.
		if (getColorName(S3) == colorBlue){
			color = "Blue";
			displayColor(color);
			turn90right(100);
			drive(0,20*1.5,50);
		}
		//If the colour is red it displays Red and turns right twice.
		else if (getColorName(S3) == colorRed){
			color = "Red";
			displayColor(color);
			turn90right(100);
			drive(0,20,50);
			turn90right(100);
			drive(0,20,50);
			
		}
		//Lastly if the colour is yellow it displays Yellow.
		else if (getColorName(S3) == colorYellow){
			color = "Yellow";
			displayColor(color);
			break;		
		}
		else {
			drive(0,20*1.25,50);
		}
		
	}//End while loop
}//End Main