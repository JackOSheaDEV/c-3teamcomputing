
//Name of Motors used throughout the Program.
int leftMotor = 1;
int rightMotor = 2;

int length = 0;

//This function is responsible for making the motor go forward and backwards.
void drive(long nMotorRatio, float dist, long power)
{
	int encoderRatio = (360/2*3.14* 2.75) * dist ; // EncoderRatio = 360/Circumference * distance
	setMotorSyncEncoder(leftMotor, rightMotor,nMotorRatio, encoderRatio, power);
	sleep((500*dist/25) * (100/power)); //An issue with sleep meant that when changing power the robot would not go the full distance specified, this was fixed by adding distance and power into the function.
	//500*60/25 * 100 will go foward exactly 60cm and if you change power to half speed itll double the time delay to account for the decreased speed.
	
	//Resets Motor Speed as a precaution.
	setMotorSpeed(leftMotor, 0);
	setMotorSpeed(rightMotor, 0);
	
}



//This function extends drive and is used to turn left at a given power.
void turn90left(int power)
{
	int currenthead = getGyroDegrees(S1);
	
	while (getGyroDegrees(S1) <= currenthead+87)
	{
		sleep(100);
		drive(100,0.25,50);
		
	}
	
}

//This function extends drive and is used to turn right at a given power
void turn90right(long Power)
{
	int currenthead = getGyroDegrees(S1);
	
	//Uses Gyro to get current angle and turns in increments until 90 degrees.
	while (-getGyroDegrees(S1) <= currenthead+87)
	{
		sleep(100);
		drive(-100,0.25,50);
		
	}
	
}




task main()
{
	//Variables used throughout:
	int currentx = abs(2);
	int currenty = abs(2);
	int destinationx = abs(6);
	int destinationy = abs(6);
	
	//IMPORTANT: Using maths as we know the boxes are 30cm across.
	// we can measure the length across and divide by the total of smaller boxes to find the length is 22.5cm,
	// so everytime 22.5cm is driven x and y are incremented. 
	
	SensorType[S1]= sensorEV3_Gyro;
	//Drives forward until currentX = destinationX.
	while(currentx != destinationx)
	{
		drive(0,22.5,50);
		currentx = currentx + 1;
		
		
	}
	//If the robot needs to go upwards this code is executed.
	if((currenty - destinationy) > 0)
	{
		turn90left(50);
		//drives forward until currentY = destinationY
		while(currenty != destinationy)
		{
			drive(0,22.5,50);
			currenty = currenty - 1;	
		}
	}
	//If y is downwards this code is executed
	else
	{
		//Turns right so its pointing downwards.
		turn90right(50);
		
		//Code is executed until currentY = destinationY
		while(currenty != destinationy)
		{
			drive(0,22.5,50);
			currenty = currenty + 1;	
		}//End while
		
	}//End Else
	
	
	
	
	
	
	
	
	
	
}//End main