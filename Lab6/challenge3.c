//Name of Motors used throughout the Program.
int leftMotor = 1;
int rightMotor = 2;

//This function is responsible for making the motor go forward and backwards.
void drive(long nMotorRatio, float dist, long power)
{
	int encoderRatio = (360/2*3.14* 2.75) * dist ; // EncoderRatio = 360/Circumference * distance
	setMotorSyncEncoder(leftMotor, rightMotor,nMotorRatio, encoderRatio, power);
	sleep((500*dist/25) * (100/power)); //An issue with sleep meant that when changing power the robot would not go the full distance specified, this was fixed by adding distance and power into the function.
	//500*60/25 * 100 will go foward exactly 60cm and if you change power to half speed itll double the time delay to account for the decreased speed.
	
	//Resets Motor Speed as a precaution.
	setMotorSpeed(leftMotor, 0);
	setMotorSpeed(rightMotor, 0);
	
}



//This function extends drive and is used to turn left at a given power.
void turn90left(int power)
{
	int currenthead;
	resetGyro(S1); 
	currenthead = getGyroDegrees(S1);
	
	while (getGyroDegrees(S1) <= currenthead+87)
	{
		sleep(100);
		drive(100,0.25,50);
	}
	
}

//This function extends drive and is used to turn right at a given power
void turn90right(long Power)
{
	int currenthead;
	resetGyro(S1);
	currenthead = getGyroDegrees(S1);
	
	//Uses Gyro to get current angle and turns in increments until 90 degrees.
	while (-getGyroDegrees(S1) <= currenthead+87)
	{
		sleep(100);
		drive(-100,0.25,50);
		
	}
	
}

// function used to turn 180 degree
void turn180(long power){
	turn90right(100);
	turn90right(100);
}

// function help robot choose direction on X when it hit Y
void xTurn(int currentX,int currentY,int destinationX, int destinationY,int startX){
	if ((currentX > destinationX) && (currentY == destinationY )){
		if (startX > destinationX){
			turn90left(100);
		}
		else {
			turn90right(100);
		}
	}
	else if (currentX < destinationX && currentY == destinationY){
		if (startX < destinationX){
			turn90left(100);
		}
		else {
			turn90right(100);
		}
	}
	else if (currentX > destinationX){
		turn180(100);
	}
}

// function help robot choose direction on Y when it hit obstacle
void yTurn(int currentX,int currentY,int destinationX, int destinationY){
	if (currentY > destinationY && currentX > destinationX){
		turn90right(100);
	}
	else if (currentY > destinationY && currentX < destinationX){
		turn90left(100);
	}
	else if (currentY < destinationY && currentX < destinationX){
		turn90right(100);
	}
}


task main()
{
	//Variables used throughout:
	
/*
	// coordinates of first senario
	int startX = 1;
	int startY = 1;
	int destinationX = 5;
	int destinationY = 5;
	int currentX = startX;
	int currentY = startY;
*/
	// coordinates of second senario 	
	int startX = 5;
	int startY = 5;
	int destinationX = 1;
	int destinationY = 1;
	int currentX = startX;
	int currentY = startY;

	bool horizontalTurn = true;
	
	SensorType[S1]= sensorEV3_Gyro;
	SensorType[S2] = sensorEV3_Ultrasonic;
	
	// IMPORTANT: robot always move on x axis first then y axis then x axis
	
	// check if destination is in the opposite direction
	xTurn(startX,startY,destinationX,destinationY,startX);
	
	// keep moving untill it get to destination
	while(currentX != destinationX || currentY != destinationY)
	{	
		// detect obstacle on the way
		while ((getUSDistance(S2) > 20) && (currentX != destinationX) && (currentY != destinationY)){
			drive(0,15,50);
			
			// detect if robot just turned to move in y axis
			if (horizontalTurn){
				if (currentX > destinationX){
					currentX--;
				}
				else {
					currentX++;
				}
			}
			else {
				if (currentY > destinationY){
					currentY--;
				}
				else {
					currentY++;
				}
			}
		}
		
		// vertical turn if hit the obstacle on x axis
		// choose which way on the y axis to turn
		if ((startX != currentX) && (startY == currentY)){
			yTurn(currentX,currentY,destinationX,destinationY);
			horizontalTurn = false;
		}
		
		// when it reaches y destination choose which way on the x axis to turn
		else {
			xTurn(currentX,currentY,destinationX,destinationY,startX);
			horizontalTurn = true;
		}
		
		// keep moving from the last turn till it reaches the destination X
		while ((currentX != destinationX) && (currentY == destinationY) && horizontalTurn){
			drive(0,15,50);
			if (currentX > destinationX){
				currentX--;
			}
			else {
				currentX++;
			}
		}
		wait1Msec(2000);
		
	}
	
}

