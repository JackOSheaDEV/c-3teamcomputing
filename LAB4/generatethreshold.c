task main()
{
	//The Variables used throughout the program.
	int darkval = 0;
	int lightval = 0;
	int threshold;
	//Configure sensor for colour and light detection.
	SensorType[S1] = sensorEV3_Color;
	
	//Print statements to display instructions to the user.
	displayTextLine(1, "Press left for light");
	displayTextLine(3, "Press right for dark.");
	displayTextLine(5, "Press up for reccomended threshold");
	displayTextLine(3, "Press down to continue.");
	
	//The program ends when the down button is pressed.
	while(getButtonPress(buttonDown)!= 1)
	{
		//Input darkval to program
		if(getButtonPress(buttonRight))
		{
			darkval = getColorReflected(S1);
			displayTextLine(3, "DarkValue inputted.");
			
			
		}//end if
		
		//Input lightval to program.
		if(getButtonPress(buttonLeft))
		{
			
			lightval = getColorReflected(S1);
			displayTextLine(1, "LightValue inputted.");
			
		}//end if
		
		//Threshold value for the robot.
		if(getButtonPress(buttonUp))
		{
			threshold = (lightval + darkval) / 2;
			displayTextLine(5, "Reccomended Value: %d", threshold);
			
			
		}//end if
		
		
		
		
		
	}
	
	
	
}
