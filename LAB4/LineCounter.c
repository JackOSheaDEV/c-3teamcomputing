//Name of Motors used throughout the Program.
int leftMotor = 1;
int rightMotor = 2;


//This function is responsible for making the motor go forward and backwards.
void drive(long nMotorRatio, long dist, long power)
{
	delay(100);
	setMotorSyncEncoder(leftMotor, rightMotor,nMotorRatio,185, power);
	sleep((500*dist/25) * (100/power)); //An issue with sleep meant that when changing power the robot would not go the full distance specified, this was fixed by adding distance and power into the function.
	//500*60/25 * 100 will go foward exactly 60cm and if you change power to half speed itll double the time delay to account for the decreased speed.
	
	//Resets Motor Speed as a precaution.
	setMotorSpeed(leftMotor, 0);
	setMotorSpeed(rightMotor, 0);
	
	
}



//This function extends drive and is used to turn left at a given power.
void turn90left(long Power)
{
	drive(100,10.25,Power);
	delay(500); //Delay to distinguish multiple turns.
	
}


//This function extends drive and is used to turn right at a given power
void turn90right(long Power)
{
	drive(-100,10.25,Power);
	delay(500); //Delay to distinguish multiple turns.
	
}
task main()
{
	//Variables:
	int darkval = 0;
	int lightval = 0;
	int threshold;
	int movement;
	int lines =0;
	//Sensor used throughtout:
	SensorType[S1] = sensorEV3_Color;
	
	//Text display
	displayTextLine(1, "Press left for light");
	displayTextLine(3, "Press right for dark.");
	displayTextLine(5, "Press up for reccomended threshold");
	displayTextLine(3, "Press down to continue.");
	
	//While the program is running
	while(getButtonPress(buttonDown)!= 1)
	{
		if(getButtonPress(buttonRight))
		{
			darkval = getColorReflected(S1);
			displayTextLine(3, "DarkValue inputted.");
			
			
		}//end if
		if(getButtonPress(buttonLeft))
		{
			
			lightval = getColorReflected(S1);
			displayTextLine(1, "LightValue inputted.");
			
		}//end if
		if(getButtonPress(buttonUp))
		{
			threshold = (lightval + darkval) / 2;
			displayTextLine(5, "Reccomended Value: %d", threshold);
			movement = 1;
			
		}//end if
		
		//========================MOVEMENT===========================
		
		//Only starts program after threshold calculated.
		while(movement==1)
		{
			//Drives forward
			drive(0,5,25);
			if(getColorReflected(S1) < threshold)
			{
				//Adds 1 to lines variable and displays to robot controller.
				lines = lines + 1;
				displayTextLine(1, "Lines %d",lines);
				//Delay of 1 second to prevent recounting
				delay(1000);			
				
			}
			
			
			
			
			
			
			
			
			
		}
		
		
		
	}
	
	
	
}
