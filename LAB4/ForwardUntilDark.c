//Name of Motors used throughout the Program.
int leftMotor = 1;
int rightMotor = 2;


//This function is responsible for making the motor go forward and backwards.
void drive(long nMotorRatio, long dist, long power)
{	
	
	int encoderRatio = 185;
	setMotorSyncEncoder(leftMotor, rightMotor,nMotorRatio,encoderRatio, power);
	sleep((500*dist/25) * (100/power)); //An issue with sleep meant that when changing power the robot would not go the full distance specified, this was fixed by adding distance and power into the function.
	//500*60/25 * 100 will go foward exactly 60cm and if you change power to half speed itll double the time delay to account for the decreased speed.
	
	//Resets Motor Speed as a precaution.
	setMotorSpeed(leftMotor, 0);
	setMotorSpeed(rightMotor, 0);
	encoderRatio = 0;
	
	
}



//This function extends drive and is used to turn left at a given power.
void turn90left(long Power)
{
	drive(100,10.25,Power);
	delay(500); //Delay to distinguish multiple turns.
	
}


//This function extends drive and is used to turn right at a given power
void turn90right(long Power)
{
	drive(-100,10.25,Power);
	delay(500); //Delay to distinguish multiple turns.
	
}
task main()
{
	//Variables to be used throughout program.
	int darkval = 0;
	int lightval = 0;
	int threshold;
	int movement;
	//Sensor Declaration
	SensorType[S1] = sensorEV3_Color;
	
	//Display Instructions.
	displayTextLine(1, "Press left for light");
	displayTextLine(3, "Press right for dark.");
	displayTextLine(5, "Press up for reccomended threshold");
	displayTextLine(3, "Press down to continue.");
	
	while(getButtonPress(buttonDown)!= 1)
	{
		//Dark value input.
		if(getButtonPress(buttonRight))
		{
			darkval = getColorReflected(S1);
			displayTextLine(3, "DarkValue inputted.");
			
			
		}//end if
		
		//Light value input.
		if(getButtonPress(buttonLeft))
		{
			
			lightval = getColorReflected(S1);
			displayTextLine(1, "LightValue inputted.");
			
		}//end if
		
		//Get threshold value.
		if(getButtonPress(buttonUp))
		{
			threshold = (lightval + darkval) / 2;
			displayTextLine(5, "Reccomended Value: %d", threshold);
			movement = 1;
			
		}//end if
		
		//========================MOVEMENT===========================
		//Starts the program only when treshold is calculated.
		while(movement==1)
		{
			//Resets speeds as a precaution.
			setMotorSpeed(0,0);
			setMotorSpeed(1,0);
			//If light, drive
			if(getColorReflected(S1) > threshold)
			{
				//Drive function followed by delay
				drive(0,1,50);
				delay(100);
			}
			
			
			
			
			
			
			
			
			
		}
		
		
		
	}
	
	
	
}
