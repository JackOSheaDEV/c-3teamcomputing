int leftMotor = 1;
int rightMotor = 2;
int lightValue;

// move forward function
void move(int rotations, int speed) {
	setMotorSyncEncoder(leftMotor ,rightMotor ,0 ,rotations ,speed);
	wait1Msec(200);
}

// stick to the edge function
void followEgde(int rotations,int speed,int threshold){
	while (getColorReflected(2) > threshold) {
		setMotorSyncEncoder(leftMotor ,rightMotor ,100, rotations ,speed);
		wait1Msec(200);
	} // end while
	while (getColorReflected(2) < threshold) {
		setMotorSyncEncoder(leftMotor ,rightMotor ,-100,rotations ,speed);
		wait1Msec(200);
	} // end while
}

task main(){
	
	//Variables used throughout.
	int darkval = 0;
	int lightval = 0;
	int threshold;
	
	//Sensors used throughout:
	SensorType[2] = sensorEV3_Color;
	
	//Text Display
	displayTextLine(1, "Press left for light");
	displayTextLine(3, "Press right for dark.");
	displayTextLine(5, "Press up for reccomended threshold");
	displayTextLine(3, "Press down to continue.");
	
	//While program is running
	while(getButtonPress(buttonDown)!= 1)
	{
		if(getButtonPress(buttonRight))
		{
			darkval = getColorReflected(2);
			displayTextLine(3, "DarkValue inputted.");
			
			
		}//end if
		if(getButtonPress(buttonLeft))
		{
			
			lightval = getColorReflected(2);
			displayTextLine(1, "LightValue inputted.");
			
		}//end if
		if(getButtonPress(buttonUp))
		{
			threshold = (lightval + darkval) / 2;
			displayTextLine(5, "Reccomended Value: %d", threshold);
			
		}//end if
		
		
	}
	// an infinite loop to keep the robot running
	while (true){
		// tell the robot to stick to the edge
		followEgde(160,35,threshold);
		
		// move forward 
		move(160,40);
	}
}