// We have to declare the motor[x] index we will assign to the left and right motors. 
//They are global variables as we are using them in functions outside of main() and will not be changed
int leftMotor = 1;
int rightMotor = 2;

//Makes the robot swing left 90 degrees.
void swingLeft90degrees()
{
	setMotorSpeed(rightMotor, 50);
	setMotorSpeed(leftMotor, 100);
	
	delay(760);
	
	setMotorSpeed(rightMotor, 0);
	setMotorSpeed(leftMotor, 0);
	
	delay(1000);
}
//Makes the robot reverse.
void reverse1second()
{
	
	setMotorSpeed(rightMotor, -100);
	setMotorSpeed(leftMotor, -100);
	
	delay(1000);
	
	setMotorSpeed(rightMotor, 0);
	setMotorSpeed(leftMotor, 0);
}


//Makes the robot turn left
void turnLeft()
{
	
	setMotorSpeed(1, 50);   
	setMotorSpeed(2, -50);    
	sleep(420);            
} 
//Makes the robot turn right,
void turnRight()
{
	setMotorSpeed(1, -50);    
	setMotorSpeed(2, 50);   
	sleep(420);           
} 
//Makes the robot go forward one second
void goForward1second(){
	setMotorSpeed(leftMotor,100); 
	setMotorSpeed(rightMotor,100);
	sleep(1000);
}
//Makes the robot deing 90 degrees right
void swingRight90degrees()
{
	setMotorSpeed(rightMotor, 100);
	setMotorSpeed(leftMotor, 50);
	
	delay(760);
	
	setMotorSpeed(rightMotor, 0);
	setMotorSpeed(leftMotor, 0);
	
	delay(1000);
}

//Main function to demonstrate robot movement
task main()
{
	swingRight90degrees();
	swingLeft90degrees();
	reverse1second();
	goForward1second();	
	turnLeft();
	turnRight();
	
}