//This file creates a custom function which moves a two motored robot
//The code is commented out to allow you to test each part seperately

//Two motors used
int motorLeft = 1;
int motorRight = 2;

//Robot movement function
void moveRobot(long nMotorRatio, long time, long power)
{
	setMotorSyncTime(motorLeft, motorRight, nMotorRatio , time,power);
	delay(time);
	
}
//Main function
task main()
{
/*
	//Part A: Go in a square and return to original position.
	//Author: Aaron Pender
	moveRobot(0,2500,100);
	moveRobot(100,195,100);
	moveRobot(0,1250,100);
	moveRobot(100,195,100);
	moveRobot(0,1250,100);
	moveRobot(100,195,100);
	moveRobot(0,1250,100);
	moveRobot(100,195,100); */	
	
	
	//Part B: using The ROBOTC function random
	//Author: Quang Anh Nguyen
	moveRobot(0,random(1500), random(100));
	
	
/*
	// Part C:  Go forward for 2 seconds ...
	//Author: Jack O'Shea
	moveRobot(0,2000,100);
	moveRobot(100,380,100);
	moveRobot(0,4000,50);	
	*/	
	
}