
//Motors used in the project.
int leftMotor = 1;
int rightMotor = 2;

//Function to drive forward:
void drive(long nMotorRatio, long dist, long power)
{
	int encoderRatio = (360/2*3.14* 2.75) * dist ; // EncoderRatio = 360/Circumference * distance
	setMotorSyncEncoder(leftMotor, rightMotor,nMotorRatio, encoderRatio, power);
	sleep((500*dist/25) * (100/power)); //An issue with sleep meant that when changing power the robot would not go the full distance specified, this was fixed by adding distance and power into the function.
	//500*60/25 * 100 will go foward exactly 60cm and if you change power to half speed itll double the time delay to account for the decreased speed.
	setMotorSpeed(leftMotor, 0);
	setMotorSpeed(rightMotor, 0);//Resets speed as a precaution.
	delay(500);
}

//Function extends drive with custom paramaters to turn left.
void turn90left(long Power)
{
	drive(100,10,Power);
	delay(500);
	
}
//Function extends drive with custom paramaters to turn right.
void turn90right(long Power)
{
	drive(-100,10,Power);
	delay(500);
}

//Turns 180 degrees using the left function parameters * 2
void turn180(long Power)
{
	drive(-100,10*2,Power);
	delay(500);
}


//Main Function:
task main()
{	
	//PART 1 ================================================================
	//Random number generated to decide whether it does a right or left turn.
	short x = random(1);

	if(x==1)
	{
		drive(0, 50, 100);
		turn90left(95);
		drive(0, 50, 100);
		turn90left(95);
		drive(0, 50, 100);
		turn90left(95);
		drive(0, 50, 100);
		turn90left(95);	
	}
	
	if(x==2)
	{
		drive(0, 50, 100);
		turn90right(95);
		drive(0, 50, 100);
		turn90right(95);
		drive(0, 50, 100);
		turn90right(95);
		drive(0, 50, 100);
		turn90right(95);	
	}
	
	//PART 2 =====================================================================
	//Goes forward and then turns and goes backward at quarter speed.
	
	drive(0,100,100);
	turn180(95);
	drive(0,100,25);
}
