//Motors used throughout the problem.
int leftMotor = 1;
int rightMotor = 2;

//Function to drive forward:
void drive(long nMotorRatio, long dist, long power)
{
	int encoderRatio = (360/23.14*2.75) * dist ; // EncoderRatio = 360/Circumference * distance
	setMotorSyncEncoder(leftMotor, rightMotor,nMotorRatio, encoderRatio, power);
	sleep((500*dist/25) * (100/power)); //An issue with sleep meant that when changing power the robot would not go the full distance specified, this was fixed by adding distance and power into the function.
	//500*60/25 * 100 will go foward exactly 60cm and if you change power to half speed itll double the time delay to account for the decreased speed.
	setMotorSpeed(leftMotor, 0);
	setMotorSpeed(rightMotor, 0);//Resets speed as a precaution.
	delay(500);
}

task main()
{
	//Number for infinite loop.
	int num = 1;
	short rand = 0;
	//Main Logic
	while(num == 1)
	{
		//Creates a random number and turns it to a product of 25 for speed
		rand = random(3);
		rand = (rand+1)*25;//+1 is used because for products of 25 if the number is 0 it is increased to 1 so a power is supplied.
		
		//Checks if up button is pressed
		if(getButtonPress(buttonUp))
		{
			drive(0,10,rand);
			
		}//end if
		
		
		//Checks if right button is pressed
		if(getButtonPress(buttonRight))
		{
			drive(0,40,rand);
		}//end if
		
		
		//Checks if down button is pressed
		if(getButtonPress(buttonDown))
		{
			drive(0,60,rand);
		}//end if
		
		//Checks if left button is pressed
		if(getButtonPress(buttonLeft))
		{
			drive(0,80,rand);
		}//end if
		
		
		
		
		
		
		
		
		
		//Delay to prevent multiple instructions
		delay(200);
	}//End While
}//end main

