
//Name of Motors used throughout the Program.
int leftMotor = 1;
int rightMotor = 2;

int length = 0;

//This function is responsible for making the motor go forward and backwards.
void drive(long nMotorRatio, float dist, long power)
{
	int encoderRatio = (360/2*3.14* 2.75) * dist ; // EncoderRatio = 360/Circumference * distance
	setMotorSyncEncoder(leftMotor, rightMotor,nMotorRatio, encoderRatio, power);
	sleep((500*dist/25) * (100/power)); //An issue with sleep meant that when changing power the robot would not go the full distance specified, this was fixed by adding distance and power into the function.
	//500*60/25 * 100 will go foward exactly 60cm and if you change power to half speed itll double the time delay to account for the decreased speed.
	
	//Resets Motor Speed as a precaution.
	setMotorSpeed(leftMotor, 0);
	setMotorSpeed(rightMotor, 0);
	
}



//This function extends drive and is used to turn left at a given power.
void turn90left(int power)
{
	int currenthead = getGyroDegrees(S1);
	
	//Uses Gyro to get current angle and turns in increments until 90 degrees.
	while (getGyroDegrees(S1) <= currenthead+87)
	{
		sleep(100);
		drive(100,0.25,50);
		
	}
	
}

//This function extends drive and is used to turn right at a given power
void turn90right(long Power)
{
	drive(-100,10.25,Power);
	delay(500); //Delay to distinguish multiple turns.
	
}




task main()
{
	
	
	int option = 0;
	int status = 0;
	int status2 = 0;
	SensorType[S1]= sensorEV3_Gyro;
	SensorType[S4] = sensorEV3_Touch;
	SensorType[S2] = sensorEV3_Ultrasonic;
	status = SensorValue[S2];
	
	//PART 1 ================================================================
	//Ultrasonic drives until a wall is present.
	while(status>10)
	{
		//Continiously updates status of ultrasonic.
		status = SensorValue[S2];
		
		drive(0,1,100);
		
	}
	//Does a left turn 
	turn90left(50);
	
	//PART 2 ===============================================================
	
	//Gets the value of touch sensor.
	status = getTouchValue(S4);
	while(status != 1)
	{
		//Drives until it hits the wall
		status = getTouchValue(S4);
		drive(0,1,100);
		
	}
	
	//Turn 90 degrees
	setMotorSpeed(leftMotor,-100);
	setMotorSpeed(rightMotor,-100);
	delay(100);
	turn90left(50);
	
	
	//PART 3 ===============================================================
	
	//Gets value of both sensors.
	status = SensorValue[S2];
	status2 = SensorValue[S4];
	
	//Stops when either US or touch sensor is tripped.
	while(status>10 || status2 != 0)
	{
		status = SensorValue[S2];
		
		drive(0,1,100);
		
	}
	
	//Final Turn
	turn90left(50);
	
	//Part 4 ===============================================================
	//Continiously drives forward.
	while(true)
	{
		
		drive(0,1,100);
		
	}
	
	
	
	
	
	
	
	
	
	
}
