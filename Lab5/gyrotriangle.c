
//Name of Motors used throughout the Program.
int leftMotor = 1;
int rightMotor = 2;

int length = 0;

//This function is responsible for making the motor go forward and backwards.
void drive(long nMotorRatio, float dist, long power)
{
	int encoderRatio = (360/2*3.14* 2.75) * dist ; // EncoderRatio = 360/Circumference * distance
	setMotorSyncEncoder(leftMotor, rightMotor,nMotorRatio, encoderRatio, power);
	sleep((500*dist/25) * (100/power)); //An issue with sleep meant that when changing power the robot would not go the full distance specified, this was fixed by adding distance and power into the function.
	//500*60/25 * 100 will go foward exactly 60cm and if you change power to half speed itll double the time delay to account for the decreased speed.
	
	//Resets Motor Speed as a precaution.
	setMotorSpeed(leftMotor, 0);
	setMotorSpeed(rightMotor, 0);
	
}



//This function extends drive and is used to turn left at a given power.
void turn90left(int power)
{
	int currenthead = getGyroDegrees(S1);
	
	//Uses Gyro to get current angle and turns in increments until 90 degrees.
	while (getGyroDegrees(S1) <= currenthead+117)
	{
		sleep(100);
		drive(100,0.25,50);
		
	}
	
}

//This function extends drive and is used to turn right at a given power
void turn90right(long Power)
{
	drive(-100,10.25,Power);
	delay(500); //Delay to distinguish multiple turns.
	
}

//Gets the length of the triangle sides.
int getLength()
{	
	//30cm option
	if(getButtonPress(buttonUp) == 1)
	{
		
		length = 30;
		return 1;
		
	}
	//60cm option
	if(getButtonPress(buttonDown) == 1)
	{
		
		length = 60;
		return 1;
		
	}
	//90 cm option
	if(getButtonPress(buttonLeft) == 1)
	{
		
		length = 90;
		return 1;
		
	}
	//120 cm option
	if(getButtonPress(buttonRight) == 1)
	{
		
		length = 120;
		return 1;
		
	}
	
	return 0;
}


task main()
{
	int option = 0;
	SensorType[S1]= sensorEV3_Gyro;
	
	
	
	
	
	
	//Text Display
	displayTextLine(1, "Press up for 30cm");
	displayTextLine(3, "Press down for 60cm");
	displayTextLine(5, "Press left for 90cm");
	displayTextLine(7, "Press right for 120cm.");
	
	//Loops until user input has been entered.
	while(option == 0)
	{	
		option = getLength();
		delay(100);
	}
	
	//Triangle code 
	drive(0,length,100);
	turn90left(100);
	drive(0,length,100);
	turn90left(100);
	drive(0,length,100);
	turn90left(100);
	
	
	
	
	
	
	
	
}
