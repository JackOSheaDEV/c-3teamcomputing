

int leftMotor = 1;
int rightMotor = 2;

// function used to make the robot turn at the same spot
void drive(long nMotorRatio, long dist, long power)
{
	int encoderRatio = (360/2*3.14* 2.75) * dist ; // EncoderRatio = 360/Circumference * distance
	setMotorSyncEncoder(leftMotor, rightMotor,nMotorRatio, encoderRatio, power);
	sleep((500*dist/25) * (100/power)); //An issue with sleep meant that when changing power the robot would not go the full distance specified, this was fixed by adding distance and power into the function.
	//500*60/25 * 100 will go foward exactly 60cm and if you change power to half speed itll double the time delay to account for the decreased speed.
	
	//Resets Motor Speed as a precaution.
	setMotorSpeed(leftMotor, 0);
	setMotorSpeed(rightMotor, 0);
	
}

// function used to make the robot turn at the same spot
void turn(float degree){
	int currenthead = getGyroDegrees(S1);
	
	while (getGyroDegrees(S1) <= currenthead + degree - 3)
	{
		sleep(200);
		drive(100,1,100);
		
	}
	
}

// function used to record the length of edges
float readEdge(){
	return getUSDistance(S2);
}

// function used to calculate the area of triangle
float calculateArea(float edge1, float degree, float edge2){
	return 0.5*edge1*edge2*sinDegrees(degree);
}

task main()
{	
	
	// initialize some essential variables for the program
	float edge1 = 0;
	float edge2 = 0;
	float degree = 40;
	
	// set up the sensors
	SensorType[S1] = sensorEV3_Gyro;
	SensorType[S2] = sensorEV3_Ultrasonic;
	
	// Text display
	displayTextLine(1, "Press up to get the first edge");
	displayTextLine(3, "Press left to turn ");
	displayTextLine(5, "Press down to get the second edge");
	displayTextLine(7, "Press center to calculate and distplay the triangle area.");
	
	while (true) {
		
		// collect the button press from user
		waitForButtonPress();
		
		// if up button is pressed, record the length of first edge
		if (getButtonPress(buttonUp) == 1){
			edge1 = readEdge();
		}
		
		// collect the button press from user
		waitForButtonPress();
		
		// if left button is pressed, turn 40 degrees
		if (getButtonPress(buttonLeft)== 1){
			turn(degree);
		}
		
		// collect the button press from user
		waitForButtonPress();
		
		// if down button is pressed, record the length of second edge	
		if (getButtonPress(buttonDown)==1){
			edge2 = readEdge();
		}
		
		// collect the button press from user
		waitForButtonPress();
		
		// clear the display screen
		eraseDisplay();
		
		// if down button is pressed, calculate and display area of the triangle
		if (getButtonPress(buttonEnter) == 1){
			displayTextLine(3,"The area of triangle is: ");
			displayTextLine(5,"%.2f",calculateArea(edge1,degree,edge2));
			wait1Msec(5000);
		}
	}
	
	
}
